# Projeto challenge-api

### Este projeto foi desenvolvido utilizando as seguintes tecnologias:
* Java 17 
* Spring Boot 3
* Spring Data JPA com Hibernate
* Flyway, controlando os scripts do banco de dados
* SpringDocs com OpenAPI
* Spring Web
* Spring Test
* JUnit e Mockito

### Para rodar o projeto siga os seguintes passos:
* [Clonar o projeto clicando nesse link](https://gitlab.com/antonio.ishac/challenge-api) 
* Entrar na pasta do projeto, challenge-api 
* Dentro do projeto challenge-api, temos uma pasta chama "db", essa pasta contém um aquivo chamadado mysql-compose.yml
* Executar o comando docker para rodar um container do banco de dados MySQL: **docker-compose -f mysql-compose.yml up -d**

### Observação:
* Esse projeto está utilizando o controle de versão de banco de dados, o Flyway
* **Ao rodar o projeto ele executa o script de criação das tabelas do banco de dados**
* O caminho do script DDL para ser consultado encontra-se no seguinte caminho do projeto: **/challenge-api/src/main/resources/db/migration/V001__create-table-customer.sql**
* Link do projeto quando executado localmente: **http://localhost:8080/swagger-ui/index.html**

