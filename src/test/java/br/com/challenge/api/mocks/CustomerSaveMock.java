package br.com.challenge.api.mocks;

import br.com.challenge.api.request.CustomerRequest;
import br.com.challenge.api.request.PhoneRequest;

import java.util.ArrayList;
import java.util.List;

public class CustomerSaveMock {

	public static CustomerRequest customerRequest() {
		CustomerRequest customer = new CustomerRequest();
		customer.setName("NOME CLIENTE TESTE");
		customer.setDocumentNumber("16519441058");
		customer.setTypePerson("F");
		customer.setRg("5505523689");
		customer.setPhones(phonesSave());
		return customer;
	}

	public static CustomerRequest customerRequestDocumentNumberInvalid() {
		CustomerRequest customer = new CustomerRequest();
		customer.setName("NOME CLIENTE TESTE");
		customer.setDocumentNumber("12345678900");
		customer.setTypePerson("F");
		customer.setRg("225253658");
		customer.setPhones(phonesSave());
		return customer;
	}

	public static CustomerRequest customerRequestTypeInvalid() {
		CustomerRequest customer = new CustomerRequest();
		customer.setName("NOME CLIENTE TESTE");
		customer.setDocumentNumber("12345678900");
		customer.setTypePerson("U");
		customer.setRg("445223658");
		customer.setPhones(phonesSave());
		return customer;
	}

	public static Object customerRequestInvalidIePhysicalPerson() {
		CustomerRequest customer = new CustomerRequest();
		customer.setName("NOME CLIENTE TESTE");
		customer.setDocumentNumber("26054743058");
		customer.setTypePerson("F");
		customer.setRg("445223658");
		customer.setIe("123456");
		customer.setPhones(phonesSave());
		return customer;
	}

	public static CustomerRequest customerRequestInvalidCnpj() {
		CustomerRequest customer = new CustomerRequest();
		customer.setName("NOME CLIENTE TESTE");
		customer.setDocumentNumber("17692499000100");
		customer.setTypePerson("J");
		customer.setIe("5505523689");
		customer.setPhones(phonesSave());
		return customer;
	}

	public static CustomerRequest customerRequestInvalidRg() {
		CustomerRequest customer = new CustomerRequest();
		customer.setName("NOME CLIENTE TESTE");
		customer.setDocumentNumber("17692499000148");
		customer.setTypePerson("J");
		customer.setIe("5505523689");
		customer.setRg("112223336");
		customer.setPhones(phonesSave());
		return customer;
	}

	public static CustomerRequest customerRequestAnyMainPhone() {
		CustomerRequest customer = new CustomerRequest();
		customer.setName("NOME CLIENTE TESTE");
		customer.setDocumentNumber("26054743058");
		customer.setTypePerson("F");
		customer.setRg("445223658");
		customer.setPhones(phonesAnyMainPhoneSave());
		return customer;
	}

	public static List<PhoneRequest> phonesSave() {
		List<PhoneRequest> phones = new ArrayList<>();
		phones.add( phoneSave("11","989898888", "S"));
		phones.add( phoneSave("11","989898887", "N"));
		phones.add( phoneSave("11","989898886", "N"));
		return phones;
	}

	public static List<PhoneRequest> phonesAnyMainPhoneSave() {
		List<PhoneRequest> phones = new ArrayList<>();
		phones.add( phoneSave("11","989898888", "S"));
		phones.add( phoneSave("11","989898887", "S"));
		phones.add( phoneSave("11","989898886", "N"));
		return phones;
	}

	public static PhoneRequest phoneSave(String ddd, String phoneNumber, String mainPhone) {
		PhoneRequest phones = new PhoneRequest();
		phones.setDddNumber(ddd);
		phones.setPhoneNumber(phoneNumber);
		phones.setMainPhone(mainPhone);
		return phones;
	}
}
