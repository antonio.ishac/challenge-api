package br.com.challenge.api.mocks;

import br.com.challenge.domains.repository.entity.CustomerEntity;
import br.com.challenge.domains.repository.entity.PhoneEntity;
import org.apache.commons.lang3.math.NumberUtils;

import java.util.ArrayList;
import java.util.List;

public class CustomerEntityMock {

	public static CustomerEntity customerEntity() {
		CustomerEntity customer = new CustomerEntity();
		customer.setId(NumberUtils.LONG_ONE);
		customer.setName("NOME CLIENTE TESTE");
		customer.setDocumentNumber("16519441058");
		customer.setTypePerson("F");
		customer.setRg("5505523689");
		customer.setPhones(phonesSave());
		return customer;
	}

	public static List<PhoneEntity> phonesSave() {
		List<PhoneEntity> phones = new ArrayList<>();
		phones.add( phone(1L,"11","989898888", "S"));
		phones.add( phone(2L,"11","989898887", "N"));
		phones.add( phone(3L,"11","989898886", "N"));
		return phones;
	}

	public static PhoneEntity phone(Long id, String ddd, String phoneNumber, String mainPhone) {
		PhoneEntity phone = new PhoneEntity();
		phone.setId(id);
		phone.setDddNumber(ddd);
		phone.setPhoneNumber(phoneNumber);
		phone.setMainPhone(mainPhone);
		return phone;
	}

}
