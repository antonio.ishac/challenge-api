package br.com.challenge.api.controller;

import br.com.challenge.ChallengeApiApplication;
import br.com.challenge.domains.config.handler.dto.ResponseErrorDTO;
import br.com.challenge.domains.repository.CustomerRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.Assert.assertNotNull;

@ActiveProfiles(profiles = { "test" })
@TestPropertySource(locations = "classpath:application-test.properties")
@SpringBootTest(classes = { ChallengeApiApplication.class })
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class CustomerDeleteControllerTest {

    private final static String URL = "/v1/customers";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private CustomerRepository customerRepository;

    @Test
    void test_try_remove_a_customer_success() throws Exception {

        BDDMockito.when( customerRepository.existsById(ArgumentMatchers.anyLong())).thenReturn( Boolean.TRUE );

        BDDMockito
                .doAnswer( new Answer<Void>() {
                    @Override
                    public Void answer(InvocationOnMock invocation) throws Throwable {
                        return null;
                    }})
                .when(customerRepository).deleteById(ArgumentMatchers.anyLong());


        MvcResult result = this.mockMvc.perform(
                        MockMvcRequestBuilders
                                .delete( URL.concat("/1"))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers
                        .status()
                        .isNoContent())
                .andReturn();

        String contentAsString = result.getResponse().getContentAsString();

        assertNotNull(contentAsString);
    }

    @Test
    void test_try_remove_a_customer_not_found() throws Exception {

        BDDMockito.when( customerRepository.existsById(ArgumentMatchers.anyLong()) ).thenReturn( Boolean.FALSE );

        MvcResult result = this.mockMvc.perform(
                        MockMvcRequestBuilders
                                .delete(URL.concat("/2"))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers
                        .status()
                        .isBadRequest())
                .andReturn();

        String contentAsString = result.getResponse().getContentAsString();

        ResponseErrorDTO response = objectMapper.readValue(contentAsString,ResponseErrorDTO.class);

        assertNotNull(response.getError());
    }
}
