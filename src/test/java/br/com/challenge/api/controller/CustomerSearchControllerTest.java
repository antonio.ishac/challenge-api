package br.com.challenge.api.controller;

import br.com.challenge.ChallengeApiApplication;
import br.com.challenge.api.mocks.CustomerEntityMock;
import br.com.challenge.domains.config.handler.dto.ResponseErrorDTO;
import br.com.challenge.domains.repository.CustomerRepository;
import br.com.challenge.domains.repository.entity.CustomerEntity;
import br.com.challenge.domains.service.filter.CustomerFilter;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ActiveProfiles(profiles = { "test" })
@TestPropertySource(locations = "classpath:application-test.properties")
@SpringBootTest(classes = { ChallengeApiApplication.class })
@AutoConfigureMockMvc
@RunWith(SpringJUnit4ClassRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class CustomerSearchControllerTest {

    private final static String URL = "/v1/customers";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CustomerRepository customerRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void test_find_with_response_empty() throws Exception {

        Page<CustomerEntity> pageCustomer
                = new PageImpl<>(new ArrayList<>() , PageRequest.of(0, 15, Sort.by("name")) , 0L);

        BDDMockito.when(customerRepository.findByCustomerFilter( ArgumentMatchers.any(CustomerFilter.class), ArgumentMatchers.any(Pageable.class))).thenReturn(pageCustomer);

        MvcResult result = this.mockMvc.perform(
                        MockMvcRequestBuilders
                                .get(URL)
                                .param("name", "NAME TESTE")
                                .param("page", "0")
                                .param("size", "15")
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers
                        .status()
                        .isNoContent())
                .andReturn();

        assertEquals(HttpStatus.NO_CONTENT.value(), result.getResponse().getStatus());
    }

    @Test
    void test_find_with_response_not_empty() throws Exception {

        ArrayList<CustomerEntity> content = new ArrayList<>();
        content.add(CustomerEntityMock.customerEntity());
        Page<CustomerEntity> pageCustomer
                = new PageImpl<>(content ,PageRequest.of(0, 15, Sort.by("name")) , 0L);

        BDDMockito.when(customerRepository.findByCustomerFilter( ArgumentMatchers.any(CustomerFilter.class), ArgumentMatchers.any(Pageable.class))).thenReturn(pageCustomer);

        MvcResult result = this.mockMvc.perform(
                        MockMvcRequestBuilders
                                .get(URL)
                                .param("name", "NAME TESTE")
                                .param("page", "0")
                                .param("size", "15")
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers
                        .status()
                        .isOk())
                .andReturn();

        assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
    }

    @Test
    void test_find_with_response_with_id_success() throws Exception {

        BDDMockito.when(customerRepository.findById(ArgumentMatchers.anyLong())).thenReturn(Optional.of(CustomerEntityMock.customerEntity()));

        MvcResult result = this.mockMvc.perform(
                        MockMvcRequestBuilders
                                .get(URL.concat("/1"))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers
                        .status()
                        .isOk())
                .andReturn();

        assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
    }

    @Test
    void test_find_with_response_with_id_not_found() throws Exception {

        BDDMockito.when(customerRepository.findById(ArgumentMatchers.anyLong())).thenReturn(Optional.empty());

        MvcResult result = this.mockMvc.perform(
                        MockMvcRequestBuilders
                                .get(URL.concat("/1"))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers
                        .status()
                        .isBadRequest())
                .andReturn();

        String contentAsString = result.getResponse().getContentAsString();

        ResponseErrorDTO response = objectMapper.readValue(contentAsString, ResponseErrorDTO.class);

        assertNotNull(response.getError());
    }
}
