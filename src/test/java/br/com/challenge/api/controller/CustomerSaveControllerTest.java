package br.com.challenge.api.controller;

import br.com.challenge.ChallengeApiApplication;
import br.com.challenge.api.mocks.CustomerEntityMock;
import br.com.challenge.api.mocks.CustomerSaveMock;
import br.com.challenge.domains.config.handler.dto.ResponseErrorDTO;
import br.com.challenge.domains.repository.CustomerRepository;
import br.com.challenge.domains.repository.entity.CustomerEntity;
import br.com.challenge.domains.service.dto.CustomerDTO;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Optional;

import static org.junit.Assert.assertNotNull;

@ActiveProfiles(profiles = { "test" })
@TestPropertySource(locations = "classpath:application-test.properties")
@SpringBootTest(classes = { ChallengeApiApplication.class })
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@FixMethodOrder(MethodSorters.JVM)
class CustomerSaveControllerTest {

    private final static String URL = "/v1/customers";
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private CustomerRepository customerRepository;

    @Test
    void test_save_customer_with_success() throws Exception {

        BDDMockito.when( customerRepository.save(ArgumentMatchers.any(CustomerEntity.class))).thenReturn( CustomerEntityMock.customerEntity());

        MvcResult result = this.mockMvc.perform(
                        MockMvcRequestBuilders
                                .post(URL)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(CustomerSaveMock.customerRequest())))
                .andExpect(MockMvcResultMatchers
                        .status()
                        .isCreated())
                .andReturn();

        String contentAsString = result.getResponse().getContentAsString();

        CustomerDTO response = objectMapper.readValue(contentAsString,
                new TypeReference<CustomerDTO>() {
                });
        assertNotNull(response.getId());
    }

    @Test
    void test_try_save_customer_invalid_document_returned_exception() throws Exception {

        MvcResult result = this.mockMvc.perform(
                        MockMvcRequestBuilders
                                .post(URL)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(CustomerSaveMock.customerRequestDocumentNumberInvalid())))
                .andExpect(MockMvcResultMatchers
                        .status()
                        .isBadRequest())
                .andReturn();

        String contentAsString = result.getResponse().getContentAsString();

        ResponseErrorDTO response = objectMapper.readValue(contentAsString, ResponseErrorDTO.class);
        assertNotNull(response.getError());
    }

    @Test
    void test_try_save_customer_invalid_type_returned_exception() throws Exception {

        MvcResult result = this.mockMvc.perform(
                        MockMvcRequestBuilders
                                .post(URL)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(CustomerSaveMock.customerRequestTypeInvalid())))
                .andExpect(MockMvcResultMatchers
                        .status()
                        .isBadRequest())
                .andReturn();

        String contentAsString = result.getResponse().getContentAsString();

        ResponseErrorDTO response = objectMapper.readValue(contentAsString, ResponseErrorDTO.class);
        assertNotNull(response.getError());
    }

    @Test
    void test_try_save_customer_invalid_ie_with_type_physical_person_returned_exception() throws Exception {

        MvcResult result = this.mockMvc.perform(
                        MockMvcRequestBuilders
                                .post(URL)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(CustomerSaveMock.customerRequestInvalidIePhysicalPerson())))
                .andExpect(MockMvcResultMatchers
                        .status()
                        .isBadRequest())
                .andReturn();

        String contentAsString = result.getResponse().getContentAsString();

        ResponseErrorDTO response = objectMapper.readValue(contentAsString, ResponseErrorDTO.class);
        assertNotNull(response.getError());
    }

    @Test
    void test_try_save_customer_document_exists_returned_exception() throws Exception {

        BDDMockito.when(customerRepository.findByDocumentNumber(ArgumentMatchers.anyString())).thenReturn(Optional.of(CustomerEntityMock.customerEntity()));

        MvcResult result = this.mockMvc.perform(
                        MockMvcRequestBuilders
                                .post(URL)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(CustomerSaveMock.customerRequest())))
                .andExpect(MockMvcResultMatchers
                        .status()
                        .isBadRequest())
                .andReturn();

        String contentAsString = result.getResponse().getContentAsString();

        ResponseErrorDTO response = objectMapper.readValue(contentAsString, ResponseErrorDTO.class);
        assertNotNull(response.getError());
    }

    @Test
    void test_try_save_customer_cnpj_invalid_returned_exception() throws Exception {

        MvcResult result = this.mockMvc.perform(
                        MockMvcRequestBuilders
                                .post(URL)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(CustomerSaveMock.customerRequestInvalidCnpj())))
                .andExpect(MockMvcResultMatchers
                        .status()
                        .isBadRequest())
                .andReturn();

        String contentAsString = result.getResponse().getContentAsString();

        ResponseErrorDTO response = objectMapper.readValue(contentAsString, ResponseErrorDTO.class);
        assertNotNull(response.getError());
    }

    @Test
    void test_try_save_customer_cnpj_invalid_rg_returned_exception() throws Exception {

        MvcResult result = this.mockMvc.perform(
                        MockMvcRequestBuilders
                                .post(URL)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(CustomerSaveMock.customerRequestInvalidRg())))
                .andExpect(MockMvcResultMatchers
                        .status()
                        .isBadRequest())
                .andReturn();

        String contentAsString = result.getResponse().getContentAsString();

        ResponseErrorDTO response = objectMapper.readValue(contentAsString, ResponseErrorDTO.class);
        assertNotNull(response.getError());
    }

    @Test
    void test_try_save_customer_any_main_phone_returned_exception() throws Exception {

        MvcResult result = this.mockMvc.perform(
                        MockMvcRequestBuilders
                                .post(URL)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(CustomerSaveMock.customerRequestAnyMainPhone())))
                .andExpect(MockMvcResultMatchers
                        .status()
                        .isBadRequest())
                .andReturn();

        String contentAsString = result.getResponse().getContentAsString();

        ResponseErrorDTO response = objectMapper.readValue(contentAsString, ResponseErrorDTO.class);
        assertNotNull(response.getError());
    }
}
