package br.com.challenge.domains.service.mapper;

import br.com.challenge.domains.repository.entity.PhoneEntity;
import br.com.challenge.domains.service.dto.PhoneDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface PhoneMapper extends ConverterMapper<PhoneDTO, PhoneEntity> {

}
