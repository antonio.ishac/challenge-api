package br.com.challenge.domains.service.impl;

import br.com.challenge.api.request.PhoneUpdateRequest;
import br.com.challenge.domains.config.handler.exception.ApiException;
import br.com.challenge.domains.config.handler.util.ExceptionCodeEnum;
import br.com.challenge.domains.repository.PhoneRepository;
import br.com.challenge.domains.repository.entity.CustomerEntity;
import br.com.challenge.domains.service.PhoneService;
import br.com.challenge.domains.service.mapper.PhoneUpdateMapper;
import br.com.challenge.domains.service.validator.PhoneValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class PhoneServiceImpl implements PhoneService {

    private final PhoneRepository repository;
    private final PhoneUpdateMapper mapper;

    private final PhoneValidator validator;

    @Transactional
    @Override
    public void update(Long customerId, Long phoneId, PhoneUpdateRequest request) {

        var customer = new CustomerEntity();
        customer.setId(customerId);
        var phonesOpt = repository.findByIdAndCustomer(phoneId, customer);

        phonesOpt.ifPresentOrElse(
                value -> {
                    var phone = mapper.toEntity(request);
                    phone.setMainPhone(value.getMainPhone());
                    phone.setCustomer(customer);
                    repository.updatePhoneCustomerById(phone.getDddNumber(), phone.getPhoneNumber(), value.getId());
                },() -> {throw new ApiException(ExceptionCodeEnum.ERROR_SEARCH_PHONE_CUSTOMER);}
        );
    }
}
