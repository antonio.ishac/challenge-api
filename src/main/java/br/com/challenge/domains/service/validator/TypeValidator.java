package br.com.challenge.domains.service.validator;

import br.com.challenge.api.request.CustomerRequest;
import br.com.challenge.domains.config.handler.exception.ApiException;
import br.com.challenge.domains.config.handler.util.ExceptionCodeEnum;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TypeValidator {

    private static final String PHYSICAL_PERSON = "F";
    private static final String LEGAL_PERSON = "J";

    public void validateType(String typePerson) {

        if (!typePerson.equalsIgnoreCase(PHYSICAL_PERSON) &&
                !typePerson.equalsIgnoreCase(LEGAL_PERSON)) {
            throw new ApiException(ExceptionCodeEnum.ERROR_TYPE_PERSON_INVALID);
        }
    }

    public void validateTypePhysicalPerson(CustomerRequest request) {

        if (request.getTypePerson().equalsIgnoreCase(PHYSICAL_PERSON)) {
            if (!DocumentNumberValidator.builder().build().isCPF(request.getDocumentNumber())) {
                throw new ApiException(ExceptionCodeEnum.ERROR_TYPE_PERSON_PHYSICAL_DOCUMENT);
            }
            else if (StringUtils.isNotEmpty(request.getIe())) {
                throw new ApiException(ExceptionCodeEnum.ERROR_TYPE_PERSON_PHYSICAL);
            }
        }
    }

    public void validateTypeLegalPerson(CustomerRequest request) {
        if (request.getTypePerson().equalsIgnoreCase(LEGAL_PERSON)) {
            if (!DocumentNumberValidator.builder().build().isCNPJ(request.getDocumentNumber())) {
                throw new ApiException(ExceptionCodeEnum.ERROR_TYPE_PERSON_LEGAL_DOCUMENT);
            }
            else if (StringUtils.isNotEmpty(request.getRg())) {
                throw new ApiException(ExceptionCodeEnum.ERROR_TYPE_PERSON_LEGAL);
            }
        }
    }

}
