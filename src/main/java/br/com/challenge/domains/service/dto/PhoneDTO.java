package br.com.challenge.domains.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class PhoneDTO {

    @EqualsAndHashCode.Include
    @Schema(name = "id", description =  "Código único do telefone do cliente")
    private Long id;

    @JsonProperty("ddd")
    @Schema(name = "ddd", description =  "DDD", example = "11")
    private String dddNumber;

    @JsonProperty("phoneNumber")
    @Schema(name = "phoneNumber", description =  "Numero do Telefone", example = "985874589")
    private String phoneNumber;

    @JsonProperty("mainPhone")
    @Schema(name = "mainPhone", description =  "Telefone principal", example = "S")
    private String mainPhone;

}
