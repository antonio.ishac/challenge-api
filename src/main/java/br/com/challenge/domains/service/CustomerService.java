package br.com.challenge.domains.service;

import br.com.challenge.api.request.CustomerRequest;
import br.com.challenge.domains.service.dto.CustomerDTO;
import br.com.challenge.domains.service.dto.PageDTO;
import br.com.challenge.domains.service.filter.CustomerFilter;

public interface CustomerService {

    CustomerDTO saveCustomer(CustomerRequest request);

    CustomerDTO update(Long id, CustomerRequest request);
    PageDTO<CustomerDTO> findByCustomerFilter(CustomerFilter customerFilter, Integer page , Integer size );

    void remove(Long id);

    void updateStatus(Long id, String status);

    CustomerDTO findById(Long id);
}
