package br.com.challenge.domains.service.mapper;

import br.com.challenge.domains.repository.entity.CustomerEntity;
import br.com.challenge.domains.service.dto.CustomerDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {PhoneMapper.class})
public interface CustomerMapper extends ConverterMapper<CustomerDTO, CustomerEntity> {

}
