package br.com.challenge.domains.service.validator;

import br.com.challenge.domains.config.handler.exception.ApiException;
import br.com.challenge.domains.config.handler.util.ExceptionCodeEnum;
import br.com.challenge.domains.service.filter.CustomerFilter;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TypeSearchValidator {

    private final TypeValidator typeValidator;

    private void validateSearchTypePhysicalPerson(String documentNumber) {
        if (!DocumentNumberValidator.builder().build().isCPF(documentNumber)) {
            throw new ApiException(ExceptionCodeEnum.ERROR_TYPE_PERSON_PHYSICAL_DOCUMENT);
        }
    }

    private void validateSearchTypeLegalPerson(String documentNumber) {
        if (!DocumentNumberValidator.builder().build().isCNPJ(documentNumber)) {
            throw new ApiException(ExceptionCodeEnum.ERROR_TYPE_PERSON_LEGAL_DOCUMENT);
        }
    }

    public void verifySearchTypeAndDocument(CustomerFilter customerFilter) {

        if (StringUtils.isNotBlank(customerFilter.getDocumentNumber())
                && (StringUtils.isBlank(customerFilter.getTypeCustomer()) || customerFilter.getTypeCustomer() == null)) {

            throw new ApiException(ExceptionCodeEnum.ERROR_SEARCH_DOCUMENT_NOT_SELECT_TYPE);
        }

        if (StringUtils.isNotBlank(customerFilter.getTypeCustomer())) {
            typeValidator.validateType(customerFilter.getTypeCustomer());

            if ("F".equalsIgnoreCase(customerFilter.getTypeCustomer()) && StringUtils.isNotBlank(customerFilter.getDocumentNumber())) {
                validateSearchTypePhysicalPerson(customerFilter.getDocumentNumber());
            }

            if ("J".equalsIgnoreCase(customerFilter.getTypeCustomer()) && StringUtils.isNotBlank(customerFilter.getDocumentNumber())) {
                validateSearchTypeLegalPerson(customerFilter.getDocumentNumber());
            }
        }
    }
}
