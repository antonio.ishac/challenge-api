package br.com.challenge.domains.service.filter;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CustomerFilter {

    @Schema(name = "name", description = "Nome do cliente.", example = " ")
    private String name;

    @Schema(name = "situation", description = "Situação cadastral do cliente, Ativo ou Inativo.", example = " ")
    private String situation;

    @Schema(name = "registerDate", description = "Data do cadastro do cliente.", example = " ")
    private String registerDate;

    @Schema(name = "typeCustomer", description = "Tipo de cliente F - Pessoa Física | J - Pessoa Jurídica.", example = " ")
    private String typeCustomer;

    @Schema(name = "documentNumber", description = "CPF/CNPJ do cliente - Para buscar selecione o tipo antes", example = " ")
    private String documentNumber;
}
