package br.com.challenge.domains.service.mapper;

import br.com.challenge.api.request.PhoneUpdateRequest;
import br.com.challenge.domains.repository.entity.PhoneEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface PhoneUpdateMapper extends ConverterMapper<PhoneUpdateRequest, PhoneEntity> {

}
