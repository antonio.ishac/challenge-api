package br.com.challenge.domains.service.validator;

import br.com.challenge.api.request.PhoneRequest;
import br.com.challenge.domains.config.handler.exception.ApiException;
import br.com.challenge.domains.config.handler.util.ExceptionCodeEnum;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PhoneValidator {

    private static final String MAIN_PHONE = "S";

    public void verifyMainPhone(List<PhoneRequest> phones) {
        if (phones != null) {
            var listVerify = phones.stream().filter(p -> MAIN_PHONE.equalsIgnoreCase(p.getMainPhone())).count();

            if (listVerify == 0) {
                throw new ApiException(ExceptionCodeEnum.ERROR_PHONE_NOT_MAIN);
            } else if (listVerify > 1) {
                throw new ApiException(ExceptionCodeEnum.ERROR_PHONE_ANY_MAIN);
            }
        }
    }

}
