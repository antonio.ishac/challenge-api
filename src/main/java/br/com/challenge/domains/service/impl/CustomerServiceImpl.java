package br.com.challenge.domains.service.impl;

import br.com.challenge.api.request.CustomerRequest;
import br.com.challenge.domains.config.handler.exception.ApiException;
import br.com.challenge.domains.config.handler.util.ExceptionCodeEnum;
import br.com.challenge.domains.repository.CustomerRepository;
import br.com.challenge.domains.repository.PhoneRepository;
import br.com.challenge.domains.repository.entity.CustomerEntity;
import br.com.challenge.domains.service.CustomerService;
import br.com.challenge.domains.service.dto.CustomerDTO;
import br.com.challenge.domains.service.dto.PageDTO;
import br.com.challenge.domains.service.filter.CustomerFilter;
import br.com.challenge.domains.service.mapper.CustomerMapper;
import br.com.challenge.domains.service.mapper.CustomerRequestMapper;
import br.com.challenge.domains.service.validator.PhoneValidator;
import br.com.challenge.domains.service.validator.TypeSearchValidator;
import br.com.challenge.domains.service.validator.TypeValidator;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CustomerServiceImpl implements CustomerService {

    private static final String STATUS_ACTIVE = "ATIVO";
    private static final String STATUS_INACTIVE = "INATIVO";

    private final TypeValidator typeValidator;
    private final TypeSearchValidator typeSearchValidator;
    private final PhoneValidator phoneValidator;
    private final CustomerRepository repository;
    private final PhoneRepository phoneRepository;
    private final CustomerRequestMapper requestMapper;
    private final CustomerMapper mapper;

    @Override
    public CustomerDTO saveCustomer(CustomerRequest request) {
        String documentNumber = removeSpecialsCharacters(request.getDocumentNumber());
        request.setDocumentNumber(documentNumber);

        typeValidator.validateType(request.getTypePerson());
        typeValidator.validateTypePhysicalPerson(request);
        typeValidator.validateTypeLegalPerson(request);

        verifyExistsDocumentNumber(documentNumber);

        phoneValidator.verifyMainPhone(request.getPhones());

        var customer = requestMapper.toEntity(request);

        customer.setDateRegister(LocalDate.now());
        customer.setRegistrationStatus(STATUS_ACTIVE);
        var customerSave = repository.save(customer);

        return mapper.toDto(customerSave);
    }

    @Transactional
    @Override
    public CustomerDTO update(Long id, CustomerRequest request) {
        String documentNumber = removeSpecialsCharacters(request.getDocumentNumber());
        request.setDocumentNumber(documentNumber);

        if (BooleanUtils.isNotTrue(repository.existsById(id))) {
            throw new ApiException(ExceptionCodeEnum.ERROR_TRY_UPDATE_CUSTOMER);
        }

        typeValidator.validateType(request.getTypePerson());
        typeValidator.validateTypePhysicalPerson(request);
        typeValidator.validateTypeLegalPerson(request);
        phoneValidator.verifyMainPhone(request.getPhones());

        Optional<CustomerEntity> optional = repository.findById(id);

        if (optional.isPresent()) {
            phoneRepository.deleteAllByCustomerId(id);
            var customerEntity = requestMapper.updateEntity(optional.get(), request);

            var customerPhone = new CustomerEntity();
            customerPhone.setId(id);
            customerEntity.getPhones().forEach(p -> p.setCustomer(customerPhone));

            var customer = repository.save(customerEntity);

            return mapper.toDto(customer);
        }
        throw new ApiException(ExceptionCodeEnum.ERROR_CUSTOMER_NOT_EXISTS);
    }

    @Override
    public PageDTO<CustomerDTO> findByCustomerFilter(CustomerFilter customerFilter, Integer page, Integer size) {

        typeSearchValidator.verifySearchTypeAndDocument(customerFilter);

        Pageable pageable = PageRequest.of(page, size, Sort.by("name"));

        Page<CustomerEntity> pageCustomer = repository.findByCustomerFilter(customerFilter, pageable);

        pageCustomer.getContent().forEach(c -> c.setPhones(
                c.getPhones().stream().sorted((o2, o1) -> o1.getMainPhone().compareTo(o2.getMainPhone())).toList()));

        PageDTO<CustomerDTO> pageDTO = new PageDTO<>(mapper.toDto(pageCustomer.getContent()));
        pageDTO.isFirst(pageCustomer.isFirst());
        pageDTO.isLast(pageCustomer.isLast());
        pageDTO.setNumber(pageCustomer.getNumber());
        pageDTO.setNumberOfElements(pageCustomer.getNumberOfElements());
        pageDTO.setSize(pageCustomer.getSize());
        pageDTO.setTotalElements(pageCustomer.getTotalElements());
        pageDTO.setTotalPages(pageCustomer.getTotalPages());

        return pageDTO;
    }

    @Transactional
    @Override
    public void remove(Long id) {
        if(repository.existsById(id)) {
            repository.deleteById(id);
            return;
        }
        throw new ApiException(ExceptionCodeEnum.ERROR_TRY_REMOVE_CUSTOMER);
    }

    @Override
    public void updateStatus(Long id, String status) {
        if(repository.existsById(id)) {
            repository.updateStatus(status, id);
            return;
        }
        throw new ApiException(ExceptionCodeEnum.ERROR_TRY_UPDATE_STATUS_CUSTOMER);
    }

    @Override
    public CustomerDTO findById(Long id) {
        Optional<CustomerEntity> customer = repository.findById(id);
        var customers = customer.orElseThrow( () ->  new ApiException( ExceptionCodeEnum.ERROR_CUSTOMER_NOT_EXISTS ) );
        customers.setPhones(customers.getPhones().stream().sorted((o2, o1) -> o1.getMainPhone().compareTo(o2.getMainPhone())).toList());
        return mapper.toDto(customers);
    }

    private void verifyExistsDocumentNumber(String documentNumber) {
        if (repository.findByDocumentNumber(documentNumber).isPresent()) {
            throw new ApiException(ExceptionCodeEnum.ERROR_DOCUMENT_EXISTS);
        }
    }

    private String removeSpecialsCharacters(String doc) {
        return doc.replace(".", "").replace("-", "").replace("/", "");
    }
}
