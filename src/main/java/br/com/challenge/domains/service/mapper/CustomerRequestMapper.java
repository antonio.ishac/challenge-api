package br.com.challenge.domains.service.mapper;

import br.com.challenge.api.request.CustomerRequest;
import br.com.challenge.domains.repository.entity.CustomerEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CustomerRequestMapper extends ConverterMapper<CustomerRequest, CustomerEntity> {

}
