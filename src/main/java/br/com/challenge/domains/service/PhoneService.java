package br.com.challenge.domains.service;

import br.com.challenge.api.request.PhoneUpdateRequest;

public interface PhoneService {

    void update(Long customerId, Long phoneId, PhoneUpdateRequest request);
}
