package br.com.challenge.domains.service.dto;

import br.com.challenge.api.request.PhoneRequest;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import java.time.LocalDateTime;
import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomerDTO {

    @EqualsAndHashCode.Include
    @JsonProperty("id")
    @Schema(name = "id", description = "Código único do cliente", type = "long", example = "1")
    private Long id;

    @JsonProperty("name")
    @Schema(name = "name", description = "Nome do cliente", requiredMode = Schema.RequiredMode.REQUIRED, example = "Carlos Andrade")
    private String name;

    @JsonProperty("typePerson")
    @Schema(name = "typePerson", description = "Tipo de pessoa", requiredMode = Schema.RequiredMode.REQUIRED ,example = "F")
    private String typePerson;

    @JsonProperty("documentNumber")
    @Schema(name = "documentNumber", description = "CPF/CNPJ do cliente", example = "43097334092")
    private String documentNumber;

    @JsonProperty("rg")
    @Schema(name = "rg", description = "Registro geral do cliente", example = "498856756")
    private String rg;

    @JsonProperty("ie")
    @Schema(name = "ie", description = "Inscrição estadual", example = "531558461140")
    private String ie;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("dateRegister")
    @Schema(name = "dateRegister", description = "Data do registro", type = "timestamp", example = "2023-01-01 12:45")
    private LocalDateTime dateRegister;

    @JsonProperty("registrationStatus")
    @Schema(name = "registrationStatus", description = "Status do registro cliente", example = "ATIVO")
    private String registrationStatus;

    @JsonProperty("phones")
    private List<PhoneRequest> phones;

}
