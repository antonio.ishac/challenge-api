package br.com.challenge.domains.config.handler.util;

import org.springframework.http.HttpStatus;

public enum ExceptionCodeEnum {

    INTERNAL_SERVER_ERROR(HttpStatus.INTERNAL_SERVER_ERROR),
    ERROR_TYPE_PERSON_INVALID(HttpStatus.BAD_REQUEST),
    ERROR_TYPE_PERSON_PHYSICAL(HttpStatus.BAD_REQUEST),
    ERROR_TYPE_PERSON_LEGAL(HttpStatus.BAD_REQUEST),
    ERROR_TYPE_PERSON_PHYSICAL_DOCUMENT(HttpStatus.BAD_REQUEST),
    ERROR_TYPE_PERSON_LEGAL_DOCUMENT(HttpStatus.BAD_REQUEST),
    ERROR_DOCUMENT_EXISTS(HttpStatus.BAD_REQUEST),
    ERROR_PHONE_NOT_MAIN(HttpStatus.BAD_REQUEST),
    ERROR_PHONE_ANY_MAIN(HttpStatus.BAD_REQUEST),
    ERROR_SEARCH_PHONE_CUSTOMER(HttpStatus.BAD_REQUEST),
    ERROR_TRY_UPDATE_CUSTOMER(HttpStatus.BAD_REQUEST),
    ERROR_CUSTOMER_NOT_EXISTS(HttpStatus.BAD_REQUEST),
    ERROR_TRY_REMOVE_CUSTOMER(HttpStatus.BAD_REQUEST),
    ERROR_TRY_UPDATE_STATUS_CUSTOMER(HttpStatus.BAD_REQUEST),
    ERROR_SEARCH_DOCUMENT_NOT_SELECT_TYPE(HttpStatus.BAD_REQUEST);

    private HttpStatus httpStatus;

    ExceptionCodeEnum(final HttpStatus httpStatus ) {
        this.httpStatus = httpStatus;
    }

    public String getCode() {
        return this.name();
    }

    public HttpStatus getHttpStatus() {
        return this.httpStatus;
    }
}
