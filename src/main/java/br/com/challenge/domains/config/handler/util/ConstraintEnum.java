package br.com.challenge.domains.config.handler.util;

public enum ConstraintEnum {

	UNDEFINED("UNDEFINED");

	private String name;

	private ConstraintEnum(String name ) {
		this.name = name;
	}

	public static ConstraintEnum getConstraint( String messageError ) {
		for ( ConstraintEnum v : ConstraintEnum.values() ) {
			if( messageError.contains( v.getName() )) {
				return v;
			}
		}
		return UNDEFINED;
	}

	public String getName() {
		return name;
	}
}
