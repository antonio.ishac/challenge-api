package br.com.challenge.domains.config.handler;

import br.com.challenge.domains.config.MessageConfig;
import br.com.challenge.domains.config.handler.dto.APIErrorDTO;
import br.com.challenge.domains.config.handler.dto.ApiErrorsDTO;
import br.com.challenge.domains.config.handler.dto.ApiProperties;
import br.com.challenge.domains.config.handler.dto.ResponseErrorDTO;
import br.com.challenge.domains.config.handler.util.ExceptionCodeEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.ServletWebRequest;
import java.util.Arrays;
import java.util.List;

public class APIHandler {

	protected static final String MESSAGE_DEFAULT_EXCEPTION = "Uma Exception foi lancada:";

	protected static final Logger LOG = LoggerFactory.getLogger(APIHandler.class);

    protected ApiProperties properties;
    protected MessageConfig messageConfig;

    protected ApiErrorsDTO apiErrorDTO(ExceptionCodeEnum exceptionCodeEnum) {

    	return ApiErrorsDTO
    				.builder()
    				.code( exceptionCodeEnum.getCode() )
    				.description(this.messageConfig.message(exceptionCodeEnum))
    				.build();
    }

	protected ResponseEntity<ResponseErrorDTO> responseEntity(
			final ServletWebRequest request, String version, ApiErrorsDTO message, HttpStatus httpStatus) {

		return responseEntity(request, version, Arrays.asList(message),httpStatus);
	}

	protected ResponseEntity<ResponseErrorDTO> responseEntity(
			final ServletWebRequest request, String version, List<ApiErrorsDTO> messages, HttpStatus httpStatus) {

		APIErrorDTO message = new APIErrorDTO(
				version, httpStatus.value(), messages, request.getRequest().getRequestURI(), request.getRequest().getMethod());
		return responseEntity(message);
	}

	protected ResponseEntity<ResponseErrorDTO> responseEntity(APIErrorDTO messages) {
		LOG.info("{}", messages);
		ResponseErrorDTO body = new ResponseErrorDTO(messages);
		LOG.info("{}", body );
		return ResponseEntity.status(HttpStatus.valueOf(messages.getStatus())).body(body);
	}
}
