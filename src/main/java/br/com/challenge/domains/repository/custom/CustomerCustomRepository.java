package br.com.challenge.domains.repository.custom;

import br.com.challenge.domains.repository.entity.CustomerEntity;
import br.com.challenge.domains.service.filter.CustomerFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CustomerCustomRepository {

    Page<CustomerEntity> findByCustomerFilter(CustomerFilter filter, Pageable sortedByName );
}
