package br.com.challenge.domains.repository.custom.impl;

import br.com.challenge.domains.repository.custom.CustomerCustomRepository;
import br.com.challenge.domains.repository.entity.CustomerEntity;
import br.com.challenge.domains.service.filter.CustomerFilter;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;

public class CustomerCustomRepositoryImpl implements CustomerCustomRepository {

    @PersistenceContext
    private EntityManager manager;

    @Override
    public Page<CustomerEntity> findByCustomerFilter(CustomerFilter filter, Pageable sortedByName) {
        var builder = manager.getCriteriaBuilder();
        var criteria = builder.createQuery(CustomerEntity.class);
        var root = criteria.from(CustomerEntity.class);

        var predicates = createRestrictions(filter, builder, root);
        criteria.where(predicates);
        criteria.orderBy((builder.desc(root.get("name"))));

        var query = manager.createQuery(criteria);

        addRestrictionsOfPagination(query, sortedByName );

        return new PageImpl<>(query.getResultList(), sortedByName, total(filter));
    }

    private Long total(CustomerFilter filter) {
        var builder = manager.getCriteriaBuilder();
        var criteria = builder.createQuery(Long.class);
        var root = criteria.from(CustomerEntity.class);
        var predicates = createRestrictions(filter, builder, root);
        criteria.where(predicates);
        criteria.select(builder.count(root));
        return manager.createQuery(criteria).getSingleResult();
    }

    static void addRestrictionsOfPagination(TypedQuery<CustomerEntity> query, Pageable pageable) {
        var actualPage = pageable.getPageNumber();
        var totalForPage = pageable.getPageSize();
        var firstPage = actualPage * totalForPage;

        query.setFirstResult(firstPage);
        query.setMaxResults(totalForPage);
    }

    private Predicate[] createRestrictions(CustomerFilter filter, CriteriaBuilder builder, Root<CustomerEntity> root) {
        var predicates = new ArrayList<>();

        if (StringUtils.isNotBlank(filter.getName())) {
            predicates.add(builder.like( builder.upper(root.get("name")),
                    StringUtils.upperCase("%" +filter.getName().trim() + "%")));
        }

        if (StringUtils.isNotBlank(filter.getSituation())) {
            predicates.add(builder.equal( builder.upper(root.get("registrationStatus")),
                    StringUtils.upperCase(filter.getSituation().trim())));
        }

        if (StringUtils.isNotBlank(filter.getRegisterDate())) {
            predicates.add(builder.between(root.get("dateRegister"), filter.getRegisterDate(), filter.getRegisterDate()));
        }

        if (StringUtils.isNotBlank(filter.getTypeCustomer())) {
            predicates.add(builder.equal( builder.upper(root.get("typePerson")),
                    StringUtils.upperCase(filter.getTypeCustomer().trim())));
        }

        if (StringUtils.isNotBlank(filter.getDocumentNumber())) {
            predicates.add(builder.equal( builder.upper(root.get("documentNumber")),
                    StringUtils.upperCase(filter.getDocumentNumber().trim())));
        }

        return predicates.toArray(new Predicate[predicates.size()]);
    }
}
