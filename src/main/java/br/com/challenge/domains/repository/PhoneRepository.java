package br.com.challenge.domains.repository;

import br.com.challenge.domains.repository.entity.CustomerEntity;
import br.com.challenge.domains.repository.entity.PhoneEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface PhoneRepository extends JpaRepository<PhoneEntity, Long> {

    Optional<PhoneEntity> findByIdAndCustomer(Long id, CustomerEntity customer);

    Optional<List<PhoneEntity>> findByCustomer(CustomerEntity customer);

    void deleteAllByCustomerId(Long id);

    @Transactional
    @Modifying
    @Query("update PhoneEntity p set p.dddNumber = :dddNumber, p.phoneNumber = :phoneNumber where p.id = :id")
    void updatePhoneCustomerById(
            @Param("dddNumber") String dddNumber,
            @Param("phoneNumber") String phoneNumber,
            @Param("id") Long id);

}
