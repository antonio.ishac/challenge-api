package br.com.challenge.domains.repository.entity;

import jakarta.persistence.*;
import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Table(name = "tb_customer_phone")
public class PhoneEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "ddd_number", length = 2)
    private String dddNumber;

    @Column(name = "phone_number", length = 10)
    private String phoneNumber;

    @Column(name = "main_phone", length = 14)
    private String mainPhone;

    @ToString.Exclude
    @ManyToOne
    @JoinColumn(name = "id_customer")
    private CustomerEntity customer;

}

