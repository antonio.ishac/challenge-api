package br.com.challenge.domains.repository.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.*;

import java.time.LocalDate;
import java.util.List;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name = "tb_customer")
public class CustomerEntity {

    @EqualsAndHashCode.Include
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull(message = "customer.name.not.null")
    @NotEmpty(message = "customer.name.not.empty")
    @Column(name = "name", nullable = false, length = 100)
	private String name;

    @Column(name = "type_person", length = 1)
    private String typePerson;

    @Column(name = "document_number", length = 14)
    private String documentNumber;

    @Column(name = "rg", length = 10)
    private String rg;

    @Column(name = "ie", length = 15)
    private String ie;

    @Column(name = "date_register")
    private LocalDate dateRegister;

    @Column(name = "registration_status", length = 7)
    private String registrationStatus;

    @OneToMany(mappedBy = "customer", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<PhoneEntity> phones;

    public void setPhones(List<PhoneEntity> phones) {
        if (phones != null) {
            phones.forEach((PhoneEntity phone ) -> phone.setCustomer(this));
        }
        this.phones = phones;
    }

}
