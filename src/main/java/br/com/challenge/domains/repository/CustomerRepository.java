package br.com.challenge.domains.repository;

import br.com.challenge.domains.repository.custom.CustomerCustomRepository;
import br.com.challenge.domains.repository.entity.CustomerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public interface CustomerRepository extends JpaRepository<CustomerEntity, Long>, CustomerCustomRepository {

    Optional<CustomerEntity> findByDocumentNumber(String documentNumber);

    @Transactional
    @Modifying
    @Query("update CustomerEntity c set c.registrationStatus = :status where c.id = :id")
    void updateStatus(@Param("status") String status, @Param("id") Long id);

}
