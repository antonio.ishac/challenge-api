package br.com.challenge;

import br.com.challenge.domains.config.handler.dto.ApiProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@EnableConfigurationProperties(value = {ApiProperties.class})
@SpringBootApplication
public class ChallengeApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(ChallengeApiApplication.class, args);
    }
}
