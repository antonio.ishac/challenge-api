package br.com.challenge.api.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class PhoneUpdateRequest {

    @JsonProperty("ddd")
    @Schema(name = "ddd", description =  "DDD", example = "11")
    private String dddNumber;

    @JsonProperty("phoneNumber")
    @Schema(name = "phoneNumber", description =  "Numero do Telefone", example = "985874589")
    private String phoneNumber;

}
