package br.com.challenge.api.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.util.List;

@Data
public class CustomerRequest {

    @NotNull(message = "customer.name.not.null")
    @NotEmpty(message = "customer.name.not.empty")
    @JsonProperty("name")
    @Schema(name = "name", description = "Nome do cliente", requiredMode = Schema.RequiredMode.REQUIRED, example = "Carlos Andrade")
    private String name;

    @JsonProperty("typePerson")
    @Schema(name = "typePerson", description = "Tipo de pessoa", requiredMode = Schema.RequiredMode.REQUIRED, example = "F")
    private String typePerson;

    @JsonProperty("documentNumber")
    @Schema(name = "documentNumber", description = "CPF/CNPJ do cliente", example = "43097334092")
    private String documentNumber;

    @JsonProperty("rg")
    @Schema(name = "rg", description = "Registro geral do cliente", example = "498856756")
    private String rg;

    @JsonProperty("ie")
    @Schema(name = "ie", description = "Inscrição estadual", example = "531558461140")
    private String ie;

    @JsonProperty("registrationStatus")
    @Schema(name = "registrationStatus", description = "Status do cliente: ATIVO ou INATIVO", hidden = true)
    private String registrationStatus;

    @JsonProperty("phones")
    private List<PhoneRequest> phones;

}
