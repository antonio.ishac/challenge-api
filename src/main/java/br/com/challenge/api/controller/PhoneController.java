package br.com.challenge.api.controller;

import br.com.challenge.api.request.PhoneUpdateRequest;
import br.com.challenge.domains.config.handler.dto.ResponseErrorDTO;
import br.com.challenge.domains.service.PhoneService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/phones")
@RequiredArgsConstructor
@Tag(name = "Telefones", description = "Gerenciando dados do telefone do cliente.")
public class PhoneController {

    private final PhoneService phoneService;

    @Operation(summary = "Alteração do telefone do cliente", description = "Telefone",
            responses = {
                    @ApiResponse(
                            responseCode = "204",
                            description = "Phone updated" ),
                    @ApiResponse(
                            responseCode = "400",
                            description = "Bad Request",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE ,
                                    schema = @Schema(implementation = ResponseErrorDTO.class))),
                    @ApiResponse(
                            responseCode = "500",
                            description = "Internal Server Error",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = ResponseErrorDTO.class)))
            })
    @PutMapping("/customer/{customerId}/phone/{phoneId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> updatePhoneCustomer(
            @PathVariable("customerId") @Parameter(description = "Id do cliente.", example = "1") Long customerId,
            @PathVariable("phoneId") @Parameter(description = "Id do telefone.", example = "2") Long phoneId,
            @RequestBody PhoneUpdateRequest phoneUpdateRequest) {

        phoneService.update(customerId, phoneId, phoneUpdateRequest);

        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

}
