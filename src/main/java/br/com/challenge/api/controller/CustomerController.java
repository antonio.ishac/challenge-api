package br.com.challenge.api.controller;

import br.com.challenge.api.request.CustomerRequest;
import br.com.challenge.domains.config.handler.dto.ResponseErrorDTO;
import br.com.challenge.domains.service.CustomerService;
import br.com.challenge.domains.service.dto.CustomerDTO;
import br.com.challenge.domains.service.dto.PageDTO;
import br.com.challenge.domains.service.filter.CustomerFilter;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/customers")
@RequiredArgsConstructor
@Tag(name = "Clientes", description = "Gerenciando dados de clientes.")
public class CustomerController {

    private final CustomerService service;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary = "Criação de um novo cliente", description = "Clientes", responses = {
            @ApiResponse(
                    responseCode = "201",
                    description = "Customer created"),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request",
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = ResponseErrorDTO.class))),
            @ApiResponse(
                    responseCode = "500",
                    description = "Internal Server Error",
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = ResponseErrorDTO.class)))
    })
    public ResponseEntity<CustomerDTO> saveCustomer(@Valid @RequestBody CustomerRequest customer) {
        return ResponseEntity.status(HttpStatus.CREATED).body(service.saveCustomer(customer));
    }

    @Operation(summary = "Buscando clientes.", description = "Clientes",
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Customer list"),
                    @ApiResponse(
                            responseCode = "204",
                            description = "List customer empty returned",
                            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)),
                    @ApiResponse(
                            responseCode = "400",
                            description = "Bad Request",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = ResponseErrorDTO.class))),
                    @ApiResponse(
                            responseCode = "500",
                            description = "Internal Server Error",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = ResponseErrorDTO.class)))
            })
    @GetMapping
    public ResponseEntity<PageDTO<CustomerDTO>> findByCustomerFilter(
            CustomerFilter customerFilter,
            @Parameter(description = "Número inicial da página.", example = "0") Integer page,
            @Parameter(description = "Qtde de páginas.", example = "10") Integer size) {
        PageDTO<CustomerDTO> customerDTO = service.findByCustomerFilter(customerFilter, page, size);

        return ResponseEntity.status(CollectionUtils.isEmpty(customerDTO.getContent())
                ? HttpStatus.NO_CONTENT : HttpStatus.OK).body(customerDTO);
    }

    @Operation(summary = "Atualizar status do cliente", description = "Clientes",
            responses = {
                    @ApiResponse(
                            responseCode = "204",
                            description = "No Content"),
                    @ApiResponse(
                            responseCode = "400",
                            description = "Invalid body field",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = ResponseErrorDTO.class))),
                    @ApiResponse(
                            responseCode = "500",
                            description = "Internal Server Error",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = ResponseErrorDTO.class)))
            })
    @PutMapping("/{id}/status/{status}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<CustomerDTO> updateStatus(
            @PathVariable("id") @Parameter(description = "ID do Cliente", example = "1") Long id,
            @PathVariable("status") @Parameter(description = "Status do cliente: ATIVO ou INATIVO", example = "ATIVO") String status) {

        service.updateStatus(id, status);

        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @Operation(summary = "Atualizar cliente", description = "Clientes",
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Customer updated"),
                    @ApiResponse(
                            responseCode = "400",
                            description = "Invalid body field",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = ResponseErrorDTO.class))),
                    @ApiResponse(
                            responseCode = "500",
                            description = "Internal Server Error",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = ResponseErrorDTO.class)))
            })
    @PutMapping("/{id}")
    public ResponseEntity<CustomerDTO> update(
            @PathVariable("id") @Parameter(description = "Id do cliente.", example = "1") Long id,
            @Valid @RequestBody CustomerRequest request) {

        var customerDTO = service.update(id, request);

        return ResponseEntity.status(HttpStatus.OK).body(customerDTO);
    }

    @Operation(summary = "Deletar um cliente.", description = "Clientes",
            responses = {
                    @ApiResponse(
                            responseCode = "204",
                            description = "No Content"),
                    @ApiResponse(
                            responseCode = "400",
                            description = "Bad Request",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = ResponseErrorDTO.class))),
                    @ApiResponse(
                            responseCode = "500",
                            description = "Internal Server Error",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = ResponseErrorDTO.class)))
            })

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> remove(
            @PathVariable("id") @Parameter(description = "Id do cliente.", example = "1") Long id) {
        service.remove(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @Operation(summary = "Buscando cliente pelo ID", description = "Clientes",
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Customer list"),
                    @ApiResponse(
                            responseCode = "400",
                            description = "Bad Request",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = ResponseErrorDTO.class))),
                    @ApiResponse(
                            responseCode = "500",
                            description = "Internal Server Error",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = ResponseErrorDTO.class)))
            })
    @GetMapping("/{id}")
    public ResponseEntity<CustomerDTO> findById(
            @PathVariable("id") @Parameter(description = "Id do cliente", example = "1") Long id) {

        return ResponseEntity.status(HttpStatus.OK).body(service.findById(id));
    }
}
