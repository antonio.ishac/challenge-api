CREATE TABLE tb_customer (
	`id` BIGINT PRIMARY KEY AUTO_INCREMENT,
	`name` VARCHAR(100) NOT NULL,
	`type_person` CHAR(1) NOT NULL,
	`document_number` VARCHAR(14),
	`rg` VARCHAR(10),
	`ie` VARCHAR(15),
	`date_register` DATE NOT NULL,
	`registration_status` VARCHAR(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE tb_customer_phone(
	`id` BIGINT PRIMARY KEY AUTO_INCREMENT,
	`ddd_number` VARCHAR(2) NOT NULL,
	`phone_number` VARCHAR(10) NOT NULL,
	`main_phone` CHAR(1) NOT NULL,
	`id_customer` BIGINT NOT NULL,

	FOREIGN KEY (`id_customer`) REFERENCES tb_customer(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
