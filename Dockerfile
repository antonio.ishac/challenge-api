FROM openjdk:17
VOLUME /tmp
COPY /target/*.jar /usr/app/app.jar
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/usr/app/app.jar"]